package modelo;

import java.util.List;

import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;
import dto.SignoZodiacoDTO;
import dto.TipoContactoDTO;
import persistencia.dao.interfaz.*;



public class Agenda 
{
	private PersonaDAO persona;	
	private LocalidadDAO localidad;
	private PaisDAO pais;
	private ProvinciaDAO provincia;
	private SignoZodiacoDAO signoZodiaco;
	private TipoContactoDAO tipoContacto;
	
	public Agenda(DAOAbstractFactory metodo_persistencia)
	{
		this.persona = metodo_persistencia.createPersonaDAO();
		this.localidad = metodo_persistencia.createLocalidadDAO();
		this.signoZodiaco = metodo_persistencia.createSignoZodiacoDAO();
		this.pais = metodo_persistencia.createPaisDAO();
		this.tipoContacto = metodo_persistencia.createTipoContactoDAO();
		this.provincia = metodo_persistencia.createProvinciaDAO();
	}
	
	public void agregarPersona(PersonaDTO nuevaPersona)
	{
		this.persona.insert(nuevaPersona);
	}

	public void borrarPersona(PersonaDTO persona_a_eliminar) 
	{
		this.persona.delete(persona_a_eliminar);
	}
	
	public void editarPersona(PersonaDTO persona_a_editar) 
	{
		this.persona.update(persona_a_editar);
	}
	
	public List<PersonaDTO> obtenerPersonas()
	{
		return this.persona.readAll();		
	}
	
		
	public List<LocalidadDTO> obtenerLocalidades()
	{
		return this.localidad.readAll();		
	}
	
	public List<ProvinciaDTO> obtenerProvincias()
	{
		return this.provincia.readAll();		
	}
	
	public List<PaisDTO> obtenerPaises()
	{
		return this.pais.readAll();		
	}
	
	public List<SignoZodiacoDTO> obtenerSignosZodiaco()
	{
		return this.signoZodiaco.readAll();		
	}
	
	public List<TipoContactoDTO> obtenerTiposContacto()
	{
		return this.tipoContacto.readAll();		
	}
	
}
