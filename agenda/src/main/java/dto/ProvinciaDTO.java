package dto;

import java.util.ArrayList;
import java.util.List;

public class ProvinciaDTO {
	private int idProvincia;
	private String nombre;
	private PaisDTO pais;
	private List<LocalidadDTO> localidades;

	public ProvinciaDTO(int idProvincia, String nombre, PaisDTO pais) {
		
		this.idProvincia = idProvincia;
		this.nombre = nombre;
		this.pais = pais;
		this.localidades = new ArrayList<LocalidadDTO>();
		
	}
	
	public String toString() {
		return nombre;
	}



	public int getIdProvincia() {
		return idProvincia;
	}



	public String getNombre() {
		return nombre;
	}



	public List<LocalidadDTO> getLocalidades() {
		return localidades;
	}



	public PaisDTO getPais() {
		return pais;
	}
	
	

}
