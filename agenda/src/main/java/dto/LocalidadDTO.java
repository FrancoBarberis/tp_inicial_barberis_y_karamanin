package dto;

public class LocalidadDTO {
	
	private int idLocalidad;
	private ProvinciaDTO provincia;
	private String nombre;
	
	
public LocalidadDTO(int idLocalidad, String nombre, ProvinciaDTO provincia) {

		this.idLocalidad = idLocalidad;
		this.nombre = nombre;
		this.provincia = provincia;
}

public int getIdLocalidad() {
	return idLocalidad;
}

public String getNombre() {
	return nombre;
}

public ProvinciaDTO getProvincia() {
	return provincia;
}

public String toString() {
	String localidad = this.nombre;
	String provincia = this.getProvincia().getNombre();
	String pais = this.getProvincia().getPais().getNombre();
	String ubicacion = (localidad + ", " + provincia + ", " + pais);
	
	return ubicacion;
}






}
