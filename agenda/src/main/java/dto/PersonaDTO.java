package dto;

public class PersonaDTO 
{
	private int idPersona;
	private String nombre;
	private String telefono;
	private String calle;
	private String altura;
	private String piso;
	private String dpto;
	private LocalidadDTO localidad;
	private String mail;
	private String cumpleanios;
	private TipoContactoDTO tipoContacto;
	private SignoZodiacoDTO signoZodiaco;
	private String codigoPostal;

	public PersonaDTO(int idPersona, String nombre, String telefono ,String calle, String altura, String piso, String dpto, LocalidadDTO localidad, String mail, String cumpleanios, TipoContactoDTO tipoContacto, SignoZodiacoDTO signoZodiaco, String codigoPostal)

	{
		this.idPersona = idPersona;
		this.nombre = nombre;
		this.telefono = telefono;
		this.calle = calle;
		this.altura = altura;
		this.piso = piso;
		this.dpto = dpto;
		this.localidad = localidad;
		this.mail = mail;
		this.cumpleanios = cumpleanios;
		this.tipoContacto = tipoContacto;
		this.signoZodiaco = signoZodiaco;
		this.codigoPostal = codigoPostal;

	}
	
	public int getIdPersona() 
	{
		return this.idPersona;
	}

	public void setIdPersona(int idPersona) 
	{
		this.idPersona = idPersona;
	}

	public String getNombre() 
	{
		return this.nombre;
	}

	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}

	public String getTelefono() 
	{
		return this.telefono;
	}

	public void setTelefono(String telefono) 
	{
		this.telefono = telefono;
	}
	
	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getDpto() {
		return dpto;
	}

	public void setDpto(String dpto) {
		this.dpto = dpto;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getCumpleanios() {
		return cumpleanios;
	}

	public void setCumpleanios(String cumpleanios) {
		this.cumpleanios = cumpleanios;
	}

	public LocalidadDTO getLocalidad() {
		return localidad;
	}

	public TipoContactoDTO getTipoContacto() {
		return tipoContacto;
	}



	public SignoZodiacoDTO getSignoZodiaco() {
		return signoZodiaco;
	}



	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	
	
	
	
	
	
}
