package dto;

import java.util.ArrayList;
import java.util.List;

public class PaisDTO {
	
	private int idPais;
	private String nombre;
	private List<ProvinciaDTO> provincias;
	


public PaisDTO(int idPais, String nombre) {
	
	this.idPais = idPais;
	this.nombre = nombre;
	this.provincias = new ArrayList<ProvinciaDTO>();
	
}

public String toString() {
	return nombre;
}



public int getIdPais() {
	return idPais;
}



public String getNombre() {
	return nombre;
}



public List<ProvinciaDTO> getProvincias() {
	return provincias;
}



}