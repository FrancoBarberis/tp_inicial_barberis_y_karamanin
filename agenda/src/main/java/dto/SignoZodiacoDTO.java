package dto;

public class SignoZodiacoDTO {
	
	private int idSigno;
	private String signo;
	
	public SignoZodiacoDTO(int idSigno, String signo) {

		this.idSigno = idSigno;
		this.signo = signo;
	
	}

	public int getIdSigno() {
		return idSigno;
	}

	public String getSigno() {
		return signo;
	}
	public String toString() {
		return signo;
	}
	
	
	
}
