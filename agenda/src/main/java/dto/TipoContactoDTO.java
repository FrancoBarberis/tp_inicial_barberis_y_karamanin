package dto;

public class TipoContactoDTO {
	
	private int idTipo;
	private String Tipo;
	
	public TipoContactoDTO(int idTipo, String tipo) {

		this.idTipo = idTipo;
		this.Tipo = tipo;
	}

	public int getIdTipo() {
		return idTipo;
	}

	public String getTipo() {
		return Tipo;
	}
	
	public String toString() {
		return Tipo;
	}
	
		
}
