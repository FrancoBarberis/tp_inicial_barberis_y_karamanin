package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;

import modelo.Agenda;
import presentacion.reportes.ReporteAgenda;
import presentacion.vista.VentanaPersona;
import presentacion.vista.VentanaConexion;
import presentacion.vista.VentanaEditar;
import presentacion.vista.Vista;
import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;
import dto.SignoZodiacoDTO;
import dto.TipoContactoDTO;

public class Controlador implements ActionListener
{
		private Vista vista;
		private List<PersonaDTO> personasEnTabla;
		private List<PaisDTO> paises;
		private List<ProvinciaDTO> provincias;
		private List<LocalidadDTO> localidades;
		private List<SignoZodiacoDTO> signosZodiaco;
		private List<TipoContactoDTO> tiposContacto;
		private VentanaPersona ventanaPersona;
		private VentanaEditar ventanaEditar;
		private VentanaConexion ventanaConexion;
		private Agenda agenda;
		
		private PersonaDTO persona_a_editar; 
		
		
		public Controlador(Vista vista, Agenda agenda)
		{
			this.vista = vista;
			this.vista.getBtnAgregar().addActionListener(a-> ventanaAgregarPersona(a));
			this.vista.getBtnBorrar().addActionListener(s-> borrarPersona(s));
			this.vista.getBtnReporte().addActionListener(r-> mostrarReporte(r));
			this.vista.getBtnEditar().addActionListener(e-> ventanaEditarPersona(e)); //AGREGADO
			this.ventanaConexion = ventanaConexion.getInstance();
			this.ventanaConexion.getBtnConfirmar().addActionListener(c-> confirmarDatosConexion(c));
			this.ventanaPersona = VentanaPersona.getInstance();
			this.ventanaPersona.getBtnAgregarPersona().addActionListener(p->guardarPersona(p));
			this.ventanaEditar = VentanaEditar.getInstance();
			this.ventanaEditar.getBtnEditarPersona().addActionListener(u->guardarCambios(u));
			this.agenda = agenda;
		}
		
		private void ventanaAgregarPersona(ActionEvent a) {
			this.ventanaPersona.mostrarVentana();
			
		}
		
		private void confirmarDatosConexion(ActionEvent c) {
			String ip = ventanaConexion.getTxtIP().getText();
			String puerto = ventanaConexion.getTxtPuerto().getText();
			String usuario = ventanaConexion.getTxtUsuario().getText();
			String contrasenia = new String(ventanaConexion.getTxtContrasenia().getPassword());
			
			Properties datosConexion = new Properties();
			try {
				datosConexion.load(new FileInputStream(System.getProperty("user.dir") +"/DatosConexion.txt"));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			datosConexion.setProperty("ip", ip);
			datosConexion.setProperty("puerto", puerto);
			datosConexion.setProperty("usuario", usuario);
			datosConexion.setProperty("contrasenia", contrasenia);
			
			try {
				FileOutputStream archivoDatos = new FileOutputStream(System.getProperty("user.dir") + "/DatosConexion.txt");
				datosConexion.store(archivoDatos, null);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			this.ventanaConexion.cerrar();
			this.inicializarCampos();		
			this.ventanaPersona.cargarCampos(this.signosZodiaco,this.tiposContacto,this.localidades);
			this.ventanaEditar.cargarCampos(this.signosZodiaco,this.tiposContacto,this.localidades);
			this.refrescarTabla();
			this.vista.show();
		}
		
		private void ventanaEditarPersona(ActionEvent e) {
			int fila = vista.getTablaPersonas().getSelectedRow();
			this.persona_a_editar = personasEnTabla.get(fila);
			this.ventanaEditar.llenarVentana(this.getPersona_a_editar());
			this.ventanaEditar.mostrarVentana();
		}


		private void guardarPersona(ActionEvent p) {
			
			String nombre = this.ventanaPersona.getTxtNombre().getText();
			String tel = ventanaPersona.getTxtTelefono().getText();
			String calle = ventanaPersona.getTxtCalle().getText();
			String altura = ventanaPersona.getTxtAltura().getText();
			String piso = ventanaPersona.getTxtPiso().getText();
			String dpto = ventanaPersona.getTxtDpto().getText();
			LocalidadDTO localidad = (LocalidadDTO)ventanaPersona.getTxtLocalidad().getSelectedItem();
			String mail = ventanaPersona.getTxtMail().getText();
			String cumpleanios = ventanaPersona.getTxtCumpleanios().getText();
			TipoContactoDTO tipoContacto = (TipoContactoDTO)ventanaPersona.getTxtTipoContacto().getSelectedItem();
			SignoZodiacoDTO signoZodiaco = (SignoZodiacoDTO) ventanaPersona.getTxtSignoZodiaco().getSelectedItem();
			String codigoPostal = ventanaPersona.getTxtCodigoPostal().getText();


			PersonaDTO nuevaPersona = new PersonaDTO(0, nombre, tel, calle, altura, piso, dpto, localidad, mail, cumpleanios, tipoContacto, signoZodiaco, codigoPostal);

			this.agenda.agregarPersona(nuevaPersona);
			this.refrescarTabla();
			this.ventanaPersona.cerrar();
		}
		
		private void guardarCambios(ActionEvent u) {
			int id = this.getPersona_a_editar().getIdPersona();
			String nombre = this.ventanaEditar.getTxtNombre().getText();
			String tel = ventanaEditar.getTxtTelefono().getText();
			String calle = ventanaEditar.getTxtCalle().getText();
			String altura = ventanaEditar.getTxtAltura().getText();
			String piso = ventanaEditar.getTxtPiso().getText();
			String dpto = ventanaEditar.getTxtDpto().getText();
			LocalidadDTO localidad = (LocalidadDTO)ventanaEditar.getTxtLocalidad().getSelectedItem();
			String mail = ventanaEditar.getTxtMail().getText();
			String cumpleanios = ventanaEditar.getTxtCumpleanios().getText();
			TipoContactoDTO tipoContacto = (TipoContactoDTO)ventanaEditar.getTxtTipoContacto().getSelectedItem();
			SignoZodiacoDTO signoZodiaco = (SignoZodiacoDTO) ventanaEditar.getTxtSignoZodiaco().getSelectedItem();
			String codigoPostal = ventanaEditar.getTxtCodigoPostal().getText();


			PersonaDTO persona_editada = new PersonaDTO(id, nombre, tel, calle, altura, piso, dpto, localidad, mail, cumpleanios, tipoContacto, signoZodiaco, codigoPostal);

			this.agenda.editarPersona(persona_editada);
			this.refrescarTabla();
			this.ventanaEditar.cerrar();
		}

		private void mostrarReporte(ActionEvent r) {
			ReporteAgenda reporte = new ReporteAgenda(agenda.obtenerPersonas());
			reporte.mostrar();	
		}

		public void borrarPersona(ActionEvent s)
		{
			int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
			for (int fila : filasSeleccionadas)
			{
				this.agenda.borrarPersona(this.personasEnTabla.get(fila));
			}
			
			this.refrescarTabla();
		}
		public void editarPersona(ActionEvent e) //COLOCAR FUNCIONALIDAD CORRECTA
		{
			int filaSeleccionada = this.vista.getTablaPersonas().getSelectedRow();
			
			this.agenda.borrarPersona(this.personasEnTabla.get(filaSeleccionada));
			this.refrescarTabla();
		}
		
		public void inicializar()
		{
			this.ventanaConexion.inicializar();
			this.ventanaConexion.show();
			
		}
		
		private void refrescarTabla()
		{
			this.personasEnTabla = agenda.obtenerPersonas();
			this.vista.llenarTabla(this.personasEnTabla);
		}

		@Override
		public void actionPerformed(ActionEvent e) { }
		
		
		
		public void setPersona_a_editar(PersonaDTO persona_a_editar) {
			this.persona_a_editar = persona_a_editar;
		}

		public PersonaDTO getPersona_a_editar() {
			return persona_a_editar;
		}
		
		public void inicializarCampos() {
			this.localidades = agenda.obtenerLocalidades();
			this.signosZodiaco = agenda.obtenerSignosZodiaco();
			this.tiposContacto = agenda.obtenerTiposContacto();
		
			
		}
		
	
		
		
		
}
