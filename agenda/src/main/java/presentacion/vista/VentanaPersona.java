package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.ProvinciaDTO;
import dto.SignoZodiacoDTO;
import dto.TipoContactoDTO;

import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

public class VentanaPersona extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtTelefono;
	private JTextField txtCalle;
	private JTextField txtAltura;
	private JTextField txtPiso;
	private JTextField txtDpto;
	private JComboBox<LocalidadDTO> txtLocalidad;
	private JTextField txtMail;
	private JTextField txtCumpleanios;
	private JComboBox<TipoContactoDTO> txtTipoContacto;
	private JButton btnAgregarPersona;
	private JComboBox<SignoZodiacoDTO> txtSignoZodiaco;

	
	private static VentanaPersona INSTANCE;
	private JLabel lblCodigoPostal;
	private JTextField txtCodigoPostal;
	
	public static VentanaPersona getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaPersona(); 	
			return new VentanaPersona();
		}
		else
			return INSTANCE;
	}

	private VentanaPersona() 
	{
		super();
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 697, 479);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(10, 11, 648, 414);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblCalle = new JLabel("Calle");
		lblCalle.setLocation(39, 70);
		lblCalle.setSize(105, 20);
		panel.add(lblCalle);
		
		//LABELS
		
		JLabel lblNombreYApellido = new JLabel("Nombre y apellido");
		lblNombreYApellido.setBounds(39, 19, 192, 14);
		panel.add(lblNombreYApellido);
		lblNombreYApellido.setBounds(39, 19, 113, 14);
		
		JLabel lblAltura = new JLabel("Altura");
		lblAltura.setLocation(39, 104);
		lblAltura.setSize(113, 14);
		panel.add(lblAltura);
		
		JLabel lblPiso = new JLabel("Piso");
		lblPiso.setLocation(39, 138);
		lblPiso.setSize(58, 14);
		panel.add(lblPiso);
		
		JLabel lblDto= new JLabel("Dto");
		lblDto.setLocation(39, 163);
		lblDto.setSize(44, 14);
		panel.add(lblDto);
		
		JLabel lblLocalidad = new JLabel("Localidad");
		lblLocalidad.setLocation(39, 194);
		lblLocalidad.setSize(140, 14);
		panel.add(lblLocalidad);
		
		JLabel lblMail = new JLabel("Mail");
		lblMail.setBounds(39, 228, 113, 14);
		panel.add(lblMail);
		
		JLabel lblCumpleanios = new JLabel("Cumpleanios");
		lblCumpleanios.setLocation(39, 259);
		lblCumpleanios.setSize(164, 14);
		panel.add(lblCumpleanios);
		
		JLabel lblTipoContacto = new JLabel("TipoContacto");
		lblTipoContacto.setLocation(39, 290);
		lblTipoContacto.setSize(164, 14);
		panel.add(lblTipoContacto);
		
		
		//TEXT FIELDS
		
		txtNombre = new JTextField();
		txtNombre.setBounds(258, 16, 269, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtTelefono = new JTextField();
		txtTelefono.setBounds(258, 41, 269, 20);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		txtCalle = new JTextField();
		txtCalle.setBounds(258, 70, 269, 20);
		panel.add(txtCalle);
		txtCalle.setColumns(10);
		
		txtAltura= new JTextField();
		txtAltura.setBounds(258, 101, 269, 20);
		panel.add(txtAltura);
		txtAltura.setColumns(10);

		txtPiso = new JTextField();
		txtPiso.setBounds(258, 132, 269, 20);
		panel.add(txtPiso);
		txtPiso.setColumns(10);
		
		txtDpto = new JTextField();
		txtDpto.setBounds(258, 160, 269, 20);
		panel.add(txtDpto);
		txtDpto.setColumns(10);
		
		txtLocalidad = new JComboBox<LocalidadDTO>();
		txtLocalidad.setBounds(258, 191, 269, 20);
		panel.add(txtLocalidad);
		
		txtMail = new JTextField();
		txtMail.setBounds(258, 222, 269, 20);
		panel.add(txtMail);
		txtMail.setColumns(10);
		
		txtCumpleanios= new JTextField();
		txtCumpleanios.setText("DD/MM");
		txtCumpleanios.setBounds(258, 253, 269, 20);
		panel.add(txtCumpleanios);
		txtCumpleanios.setColumns(10);
		
		txtTipoContacto= new JComboBox<TipoContactoDTO>();
		txtTipoContacto.setBounds(258, 284, 269, 20);
		panel.add(txtTipoContacto);
		
		btnAgregarPersona = new JButton("Agregar");
		btnAgregarPersona.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnAgregarPersona.setBounds(555, 380, 89, 23);
		panel.add(btnAgregarPersona);
		
		JLabel lblTelfono = new JLabel("Telefono");
		lblTelfono.setBounds(39, 44, 164, 14);
		panel.add(lblTelfono);
		
		JLabel lblSignozodiaco = new JLabel("SignoZodiaco");
		lblSignozodiaco.setBounds(39, 315, 164, 14);
		panel.add(lblSignozodiaco);
		
		txtSignoZodiaco = new JComboBox<SignoZodiacoDTO>();
		txtSignoZodiaco.setBounds(258, 312, 269, 20);
		panel.add(txtSignoZodiaco);
		
		lblCodigoPostal = new JLabel("Codigo Postal");
		lblCodigoPostal.setBounds(39, 352, 164, 14);
		panel.add(lblCodigoPostal);
		
		txtCodigoPostal = new JTextField();
		txtCodigoPostal.setColumns(10);
		txtCodigoPostal.setBounds(258, 349, 269, 20);
		panel.add(txtCodigoPostal);
		
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}

	public JTextField getTxtTelefono() 
	{
		return txtTelefono;
	}

	public JButton getBtnAgregarPersona() 
	{
		return btnAgregarPersona;
	}
	
	public JTextField getTxtCalle() {
		return txtCalle;
	}

	public JTextField getTxtAltura() {
		return txtAltura;
	}

	public JTextField getTxtPiso() {
		return txtPiso;
	}

	public JTextField getTxtDpto() {
		return txtDpto;
	}

	public JComboBox getTxtLocalidad() {
		return txtLocalidad;
	}

	public JTextField getTxtMail() {
		return txtMail;
	}

	public JTextField getTxtCumpleanios() {
		return txtCumpleanios;
	}

	public JComboBox getTxtTipoContacto() {
		return txtTipoContacto;
	}
	
	public JComboBox getTxtSignoZodiaco() {
		return txtSignoZodiaco;
	}
	
	public JTextField getTxtCodigoPostal() {
		return txtCodigoPostal;
	}

		
	
	public void cargarTiposContacto(List<TipoContactoDTO> tiposContacto) {
		TipoContactoDTO[] arrayTipos = new TipoContactoDTO[tiposContacto.size()];
		int i = 0;
		for(TipoContactoDTO tipo : tiposContacto ) {
			arrayTipos[i] = tipo;
			i++;
		}
		txtTipoContacto.setModel(new DefaultComboBoxModel<TipoContactoDTO>(arrayTipos));
 }
	
	public void cargarSignosZodiaco(List<SignoZodiacoDTO> signosZodiaco) {
		SignoZodiacoDTO[] arraySignos = new SignoZodiacoDTO[signosZodiaco.size()];
		int i = 0;
		for(SignoZodiacoDTO signo : signosZodiaco) {
			arraySignos[i] = signo;
			i++;
		}
		txtSignoZodiaco.setModel(new DefaultComboBoxModel<SignoZodiacoDTO>(arraySignos));
	}
	

	
	public void cargarLocalidades(List<LocalidadDTO> localidades){
		LocalidadDTO[] arrayLocalidades = new LocalidadDTO[localidades.size()];
		int i = 0;
		for(LocalidadDTO localidad : localidades ) {
			arrayLocalidades[i] = localidad;
			i++;
		}
		txtLocalidad.setModel(new DefaultComboBoxModel<LocalidadDTO>(arrayLocalidades));
	}
	
	public void cargarCampos(List<SignoZodiacoDTO> signos, List<TipoContactoDTO> tipos, List<LocalidadDTO> localidades) {
		this.cargarSignosZodiaco(signos);
		this.cargarTiposContacto(tipos);
		this.cargarLocalidades(localidades);
	}
	
	
	public JPanel getContentPane() {
		return contentPane;
	}

	


	public JLabel getLblCodigoPostal() {
		return lblCodigoPostal;
	}

	public void cerrar()
	{
		this.txtNombre.setText(null);
		this.txtTelefono.setText(null);
		this.txtCalle.setText(null);
		this.txtPiso.setText(null);
		this.txtAltura.setText(null);
		this.txtCumpleanios.setText("DD/MM");
		this.txtMail.setText(null);
		this.txtCodigoPostal.setText(null);
		this.txtDpto.setText(null);
		this.txtLocalidad.setSelectedIndex(1);
		this.txtTipoContacto.setSelectedIndex(1);
		this.txtSignoZodiaco.setSelectedIndex(1);

		
		this.dispose();
	}
}

