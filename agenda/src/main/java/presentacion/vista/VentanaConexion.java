package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JPasswordField;


public class VentanaConexion extends JFrame 
{

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtIP;
	private JTextField txtPuerto;
	

	private JButton btnConfirmar;
	private static VentanaConexion INSTANCE;
	private JTextField txtUsuario;
	private JPasswordField txtContrasenia;
	
	public static VentanaConexion getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaConexion(); 	
			return new VentanaConexion();
		}
		else
			return INSTANCE;
	}

	private VentanaConexion() 
	{
		super();
		setTitle("Establecer Conexion");
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 559, 329);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(10, 11, 527, 280);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblPuerto = new JLabel("Puerto");
		lblPuerto.setLocation(39, 70);
		lblPuerto.setSize(105, 20);
		panel.add(lblPuerto);
		
		//LABELS
		
		JLabel lblIP = new JLabel("Dirección de IP");
		lblIP.setBounds(39, 19, 192, 14);
		panel.add(lblIP);
		lblIP.setBounds(39, 19, 113, 14);
		
		
		
		
		
				
		txtIP = new JTextField();
		txtIP.setBounds(258, 16, 164, 20);
		panel.add(txtIP);
		txtIP.setColumns(10);
		
		txtPuerto = new JTextField();
		txtPuerto.setBounds(258, 70, 164, 20);
		panel.add(txtPuerto);
		txtPuerto.setColumns(10);
		
	
		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnConfirmar.setBounds(428, 246, 89, 23);
		panel.add(btnConfirmar);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(39, 124, 113, 14);
		panel.add(lblUsuario);
		
		JLabel lblContrasenia = new JLabel("Contrasenia");
		lblContrasenia.setBounds(39, 178, 113, 14);
		panel.add(lblContrasenia);
		
		txtUsuario = new JTextField();
		txtUsuario.setColumns(10);
		txtUsuario.setBounds(258, 121, 164, 20);
		panel.add(txtUsuario);
		
		txtContrasenia = new JPasswordField();
		txtContrasenia.setBounds(258, 175, 164, 20);
		panel.add(txtContrasenia);
		
		
		
		
		
		
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public JTextField getTxtUsuario() 
	{
		return txtUsuario;
	}

	
	public JPasswordField getTxtContrasenia() {
		return txtContrasenia;
	}

	

	public JTextField getTxtIP() {
		return txtIP;
	}

	public JTextField getTxtPuerto() {
		return txtPuerto;
	}

	public JButton getBtnConfirmar() 
	{
		return btnConfirmar;
	}
	
	public void inicializar() {
		this.txtIP.setText("localhost");
		this.txtPuerto.setText("3306");
		this.txtUsuario.setText("root");
	}
	public void cerrar()
	{
		this.txtIP.setText(null);
		this.txtPuerto.setText(null);
		this.txtUsuario.setText(null);
		this.txtContrasenia.setText(null);
		
		
		this.dispose();
	}
}
