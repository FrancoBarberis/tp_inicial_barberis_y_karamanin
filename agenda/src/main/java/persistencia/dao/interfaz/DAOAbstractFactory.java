package persistencia.dao.interfaz;


public interface DAOAbstractFactory 
{
	public PersonaDAO createPersonaDAO();
	public LocalidadDAO createLocalidadDAO();
	public ProvinciaDAO createProvinciaDAO();
	public PaisDAO createPaisDAO();
	public SignoZodiacoDAO createSignoZodiacoDAO();
	public TipoContactoDAO createTipoContactoDAO();
}
