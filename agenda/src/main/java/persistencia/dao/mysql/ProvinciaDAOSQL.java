package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.LocalidadDTO;
import dto.ProvinciaDTO;
import dto.PaisDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PaisDAO;
import persistencia.dao.interfaz.ProvinciaDAO;

public class ProvinciaDAOSQL implements ProvinciaDAO {

	
	private static final String readall = "SELECT * FROM provincias ";
	private static final String provincia_x_ID = "SELECT * FROM provincias WHERE idProvincia = ? ";


	@Override
	public List<ProvinciaDTO> readAll() {
		
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<ProvinciaDTO> provincias = new ArrayList<ProvinciaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				provincias.add(getProvinciaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return provincias;
	}
	
	private ProvinciaDTO getProvinciaDTO(ResultSet resultSet) throws SQLException{
		
		int id = resultSet.getInt("idProvincia");
		String provincia = resultSet.getString("Nombre");
		int idPais = resultSet.getInt("idPais");
		PaisDAOSQL paisSQL = new PaisDAOSQL();
		PaisDTO pais = paisSQL.obtenerPais_x_Id(idPais);
		
		
		
		return new ProvinciaDTO(id,provincia,pais);
		
	}
	
	public ProvinciaDTO obtenerProvincia_x_Id(int id) {
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		ProvinciaDTO provincia = null;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(provincia_x_ID);
			statement.setString(1, Integer.toString(id));
			resultSet = statement.executeQuery();
			resultSet.next();
			provincia  = this.getProvinciaDTO(resultSet);
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return provincia;
	}
	
	

}
