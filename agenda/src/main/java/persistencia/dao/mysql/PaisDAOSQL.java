package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.PaisDTO;
import dto.ProvinciaDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PaisDAO;

public class PaisDAOSQL implements PaisDAO{

	private static final String readall = "SELECT * FROM paises ";
	private static final String pais_x_ID = "SELECT * FROM paises WHERE idPais = ? ";


	@Override
	public List<PaisDTO> readAll() {
		
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<PaisDTO> paises = new ArrayList<PaisDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				paises.add(getPaisDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return paises;
	}
	
	private PaisDTO getPaisDTO(ResultSet resultSet) throws SQLException{
		
		int id = resultSet.getInt("idPais");
		String pais = resultSet.getString("Nombre");
		
		return new PaisDTO(id,pais);
		
	}
	
	public PaisDTO obtenerPais_x_Id(int id) {
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		PaisDTO pais = null;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(pais_x_ID);
			statement.setString(1, Integer.toString(id));
			resultSet = statement.executeQuery();
			resultSet.next();
			pais = this.getPaisDTO(resultSet);
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return pais;
	}
}
