package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.ProvinciaDTO;
import dto.SignoZodiacoDTO;
import dto.TipoContactoDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PersonaDAO;

public class PersonaDAOSQL implements PersonaDAO
{
	private static final String insert = "INSERT INTO personas(idPersona, Nombre, Telefono, Calle, Altura, Piso, Dto, idLocalidad, Mail, Cumpleanios, idTipo, idSigno, CodigoPostal) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String delete = "DELETE FROM personas WHERE idPersona = ?";
	private static final String readall = "SELECT * FROM personas ";
	private static final String update = "UPDATE personas SET Nombre = ?, Telefono = ?, Calle = ?, Altura = ?, Piso = ?, Dto = ?, idLocalidad = ?, Mail = ?, Cumpleanios = ?, idTipo = ?, idSigno = ?, CodigoPostal = ? WHERE idPersona = ?";

		
	public boolean insert(PersonaDTO persona)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, persona.getIdPersona());
			statement.setString(2, persona.getNombre());
			statement.setString(3, persona.getTelefono());
			statement.setString(4, persona.getCalle());
			statement.setString(5, persona.getAltura());
			statement.setString(6, persona.getPiso());
			statement.setString(7, persona.getDpto());
			statement.setInt(8, persona.getLocalidad().getIdLocalidad());
			statement.setString(9, persona.getMail());
			statement.setString(10, persona.getCumpleanios());
			statement.setInt(11, persona.getTipoContacto().getIdTipo());
			statement.setInt(12, persona.getSignoZodiaco().getIdSigno());
			statement.setString(13, persona.getCodigoPostal());
		

			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}
	
	public boolean update(PersonaDTO persona_a_modificar)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isUpdateExitoso = false;
		try
		{
			statement = conexion.prepareStatement(update);
			statement.setString(1, persona_a_modificar.getNombre());
			statement.setString(2, persona_a_modificar.getTelefono());
			statement.setString(3, persona_a_modificar.getCalle());
			statement.setString(4, persona_a_modificar.getAltura());
			statement.setString(5, persona_a_modificar.getPiso());
			statement.setString(6, persona_a_modificar.getDpto());
			statement.setInt(7, persona_a_modificar.getLocalidad().getIdLocalidad());
			statement.setString(8, persona_a_modificar.getMail());
			statement.setString(9, persona_a_modificar.getCumpleanios());
			statement.setInt(10, persona_a_modificar.getTipoContacto().getIdTipo());
			statement.setInt(11, persona_a_modificar.getSignoZodiaco().getIdSigno());
			statement.setString(12, persona_a_modificar.getCodigoPostal());
			statement.setInt(13, persona_a_modificar.getIdPersona());



			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isUpdateExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isUpdateExitoso;
	}
	
	public boolean delete(PersonaDTO persona_a_eliminar)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(persona_a_eliminar.getIdPersona()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	
	public List<PersonaDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				personas.add(getPersonaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return personas;
	}
	
	private PersonaDTO getPersonaDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idPersona");
		String nombre = resultSet.getString("Nombre");
		String tel = resultSet.getString("Telefono");
		String calle = resultSet.getString("Calle");
		String altura = resultSet.getString("Altura");
		String piso = resultSet.getString("Piso");
		String dpto = resultSet.getString("Dto");
		int idLocalidad = resultSet.getInt("idLocalidad");
		LocalidadDAOSQL localidadSQL = new LocalidadDAOSQL();
		LocalidadDTO localidad = localidadSQL.obtenerLocalidad_x_Id(idLocalidad);
		String mail = resultSet.getString("Mail");
		String cumpleanios = resultSet.getString("Cumpleanios");
		int idTipo = resultSet.getInt("idTipo");
		TipoContactoDAOSQL tipoContactoSQl = new TipoContactoDAOSQL();
		TipoContactoDTO tipoContacto = tipoContactoSQl.obtenerTipoContacto_x_Id(idTipo);
		int idSigno = resultSet.getInt("idSigno");
		SignoZodiacoDAOSQL signoSQL = new SignoZodiacoDAOSQL();
		SignoZodiacoDTO signoZodiaco = signoSQL.obtenerSignoZodiaco_x_Id(idSigno);
		String codigoPostal = resultSet.getString("CodigoPostal");



		return new PersonaDTO(id, nombre, tel, calle, altura, piso, dpto, localidad, mail, cumpleanios, tipoContacto, signoZodiaco, codigoPostal);
	}
}
