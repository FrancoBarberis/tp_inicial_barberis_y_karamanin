package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.LocalidadDTO;
import dto.PaisDTO;
import dto.ProvinciaDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PaisDAO;
import persistencia.dao.interfaz.ProvinciaDAO;

public class LocalidadDAOSQL implements LocalidadDAO {

	
	private static final String readall = "SELECT * FROM localidades ";
	private static final String localidad_x_ID = "SELECT * FROM localidades WHERE idLocalidad = ? ";


	@Override
	public List<LocalidadDTO> readAll() {
		
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<LocalidadDTO> localidades = new ArrayList<LocalidadDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				localidades.add(getLocalidadDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return localidades;
	}
	
	private LocalidadDTO getLocalidadDTO(ResultSet resultSet) throws SQLException{
		
		int id = resultSet.getInt("idLocalidad");
		String tipo = resultSet.getString("Nombre");
		int idProvincia = resultSet.getInt("idProvincia");
		ProvinciaDAOSQL provinciaSQL = new ProvinciaDAOSQL();
		ProvinciaDTO provincia = provinciaSQL.obtenerProvincia_x_Id(idProvincia);
		
		return new LocalidadDTO(id,tipo, provincia);
		
	}

	@Override
	public LocalidadDTO obtenerLocalidad_x_Id(int id) {
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		LocalidadDTO localidad = null;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(localidad_x_ID);
			statement.setString(1, Integer.toString(id));
			resultSet = statement.executeQuery();
			resultSet.next();
			localidad = this.getLocalidadDTO(resultSet);
			
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return localidad;
	}

}
