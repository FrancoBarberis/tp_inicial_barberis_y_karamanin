package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.SignoZodiacoDTO;
import dto.TipoContactoDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.SignoZodiacoDAO;

public class SignoZodiacoDAOSQL implements SignoZodiacoDAO {

	private static final String readall = "SELECT * FROM signosZodiaco ";
	private static final String signoZodiaco_x_ID = "SELECT * FROM signosZodiaco WHERE idSigno = ? ";

	
	public List<SignoZodiacoDTO> readAll() {
		
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<SignoZodiacoDTO> signosZodiaco = new ArrayList<SignoZodiacoDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				signosZodiaco.add(getSignoZodiacoDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return signosZodiaco;
		
	}
	
	private SignoZodiacoDTO getSignoZodiacoDTO(ResultSet resultSet) throws SQLException{
		
		int id = resultSet.getInt("idSigno");
		String signo = resultSet.getString("Nombre");
		
		return new SignoZodiacoDTO(id,signo);
		
	}
	
	public SignoZodiacoDTO obtenerSignoZodiaco_x_Id(int id) {
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		SignoZodiacoDTO signo = null;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(signoZodiaco_x_ID);
			statement.setString(1, Integer.toString(id));
			resultSet = statement.executeQuery();
			resultSet.next();
			signo = this.getSignoZodiacoDTO(resultSet);
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return signo;
	}
	
	

}
