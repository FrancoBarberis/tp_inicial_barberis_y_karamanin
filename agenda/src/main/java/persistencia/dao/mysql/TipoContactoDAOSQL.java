package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.PaisDTO;
import dto.TipoContactoDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.TipoContactoDAO;

public class TipoContactoDAOSQL implements TipoContactoDAO{

	
	private static final String tiposContacto_x_ID = "SELECT * FROM tiposContacto WHERE idTipo = ? ";
	private static final String readall = "SELECT * FROM tiposContacto ";
	
	public List<TipoContactoDTO> readAll() {
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<TipoContactoDTO> tiposContacto = new ArrayList<TipoContactoDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				tiposContacto.add(getTipoContactoDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return tiposContacto;
	}
	
	private TipoContactoDTO getTipoContactoDTO(ResultSet resultSet) throws SQLException{
		
		int id = resultSet.getInt("idTipo");
		String tipo = resultSet.getString("Nombre");
		return new TipoContactoDTO(id,tipo);
		
	}
	
	public TipoContactoDTO obtenerTipoContacto_x_Id(int id) {
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		TipoContactoDTO tipoContacto = null;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(tiposContacto_x_ID);
			statement.setString(1, Integer.toString(id));
			resultSet = statement.executeQuery();
			resultSet.next();
			tipoContacto = this.getTipoContactoDTO(resultSet);
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return tipoContacto;
	}

}
