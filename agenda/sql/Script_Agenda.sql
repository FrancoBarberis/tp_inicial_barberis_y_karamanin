DROP DATABASE IF EXISTS agenda;
CREATE DATABASE `agenda`;
USE agenda;

CREATE TABLE `personas`
(
 `idPersona` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Telefono` varchar(20) NOT NULL,
  `Calle` varchar(20) NOT NULL,
  `Altura` varchar(20) NOT NULL,
  `Piso` varchar(20),
  `Dto` varchar(20),
  `idLocalidad` int(11) NOT NULL,
  `Mail` varchar(30) NOT NULL,
  `Cumpleanios` varchar(20) NOT NULL,
  `idTipo` int(11) NOT NULL,
  `idSigno` int(11) NOT NULL,
  `CodigoPostal` varchar(20) NOT NULL,
  

  
  PRIMARY KEY (`idPersona`)
  
);

CREATE TABLE `tiposContacto`
(
	`idTipo` int(11) NOT NULL AUTO_INCREMENT,
	`Nombre` varchar(45) NOT NULL,
	
	PRIMARY KEY (`idTipo`)
);

CREATE TABLE `signosZodiaco`
(
	`idSigno` int(11) NOT NULL AUTO_INCREMENT,
	`Nombre` varchar(45) NOT NULL,
	
	PRIMARY KEY (`idSigno`)
);


CREATE TABLE `paises`
(
	`idPais` int(11) NOT NULL AUTO_INCREMENT,
	`Nombre` varchar(45) NOT NULL,
	
	PRIMARY KEY (`idPais`)
);


CREATE TABLE `provincias`
(
	`idProvincia` int(11) NOT NULL AUTO_INCREMENT,
	`idPais` int(11) NOT NULL,
	`Nombre` varchar(45) NOT NULL,
	
	PRIMARY KEY (`idProvincia`)
);

CREATE TABLE `localidades`
(
	`idLocalidad` int(11) NOT NULL AUTO_INCREMENT,
	`idProvincia` int(11) NOT NULL,
	`Nombre` varchar(45) NOT NULL,
	
	PRIMARY KEY (`idLocalidad`)
);


ALTER TABLE `personas`
    ADD CONSTRAINT `fk_Localidad`  FOREIGN KEY
    (`idLocalidad`)
    REFERENCES `localidades`(`idLocalidad`);
	
ALTER TABLE `personas`
    ADD CONSTRAINT `fk_Signo`  FOREIGN KEY
    (`idSigno`)
    REFERENCES `signosZodiaco`(`idSigno`);

ALTER TABLE `personas`
    ADD CONSTRAINT `fk_Tipo`  FOREIGN KEY
    (`idTipo`)
    REFERENCES `tiposContacto`(`idTipo`);

ALTER TABLE `localidades`
    ADD CONSTRAINT `fk_Provincia`  FOREIGN KEY
    (`idProvincia`)
    REFERENCES `provincias`(`idProvincia`);

ALTER TABLE `provincias`
    ADD CONSTRAINT `fk_Pais`  FOREIGN KEY
    (`idPais`)
    REFERENCES `paises`(`idPais`);


INSERT INTO paises(idPais,Nombre) VALUES(0, "Argentina");
INSERT INTO paises(idPais,Nombre) VALUES(0, "Uruguay");

INSERT INTO provincias(idProvincia,idPais,Nombre) VALUES(0,1,"Buenos Aires");
INSERT INTO provincias(idProvincia,idPais,Nombre) VALUES(0,1,"Cordoba");
INSERT INTO provincias(idProvincia,idPais,Nombre) VALUES(0,1,"Entre Rios");
INSERT INTO provincias(idProvincia,idPais,Nombre) VALUES(0,2,"Montevideo");
INSERT INTO provincias(idProvincia,idPais,Nombre) VALUES(0,2,"Maldonado");

INSERT INTO localidades(idLocalidad,idProvincia,Nombre) VALUES (0,1,"San Miguel");
INSERT INTO localidades(idLocalidad,idProvincia,Nombre) VALUES (0,1,"Pablo Nogues");
INSERT INTO localidades(idLocalidad,idProvincia,Nombre) VALUES (0,1,"Jose C. Paz");
INSERT INTO localidades(idLocalidad,idProvincia,Nombre) VALUES (0,1,"Muniz");
INSERT INTO localidades(idLocalidad,idProvincia,Nombre) VALUES (0,1,"Hurlingham");
INSERT INTO localidades(idLocalidad,idProvincia,Nombre) VALUES (0,2,"Cosquin");
INSERT INTO localidades(idLocalidad,idProvincia,Nombre) VALUES (0,2,"Cruz Del Eje");
INSERT INTO localidades(idLocalidad,idProvincia,Nombre) VALUES (0,2,"Cordoba");
INSERT INTO localidades(idLocalidad,idProvincia,Nombre) VALUES (0,3,"Parana");
INSERT INTO localidades(idLocalidad,idProvincia,Nombre) VALUES (0,3,"Gualeguaychu");
INSERT INTO localidades(idLocalidad,idProvincia,Nombre) VALUES (0,3,"Concordia");
INSERT INTO localidades(idLocalidad,idProvincia,Nombre) VALUES (0,4,"Ciudad Vieja");
INSERT INTO localidades(idLocalidad,idProvincia,Nombre) VALUES (0,4,"La Blanqueada");
INSERT INTO localidades(idLocalidad,idProvincia,Nombre) VALUES (0,4,"Villa Dolores");
INSERT INTO localidades(idLocalidad,idProvincia,Nombre) VALUES (0,5,"San Carlos");
INSERT INTO localidades(idLocalidad,idProvincia,Nombre) VALUES (0,5,"Punta Del Este");
INSERT INTO localidades(idLocalidad,idProvincia,Nombre) VALUES (0,5,"Piriapolis");

INSERT INTO signosZodiaco(idSigno,Nombre) VALUES (0,"Aries"); 
INSERT INTO signosZodiaco(idSigno,Nombre) VALUES (0,"Tauro"); 
INSERT INTO signosZodiaco(idSigno,Nombre) VALUES (0,"Escorpio"); 
INSERT INTO signosZodiaco(idSigno,Nombre) VALUES (0,"Sagitario"); 
INSERT INTO signosZodiaco(idSigno,Nombre) VALUES (0,"Geminis"); 
INSERT INTO signosZodiaco(idSigno,Nombre) VALUES (0,"Acuario"); 
INSERT INTO signosZodiaco(idSigno,Nombre) VALUES (0,"Piscis"); 
INSERT INTO signosZodiaco(idSigno,Nombre) VALUES (0,"Libra"); 
INSERT INTO signosZodiaco(idSigno,Nombre) VALUES (0,"Leo"); 
INSERT INTO signosZodiaco(idSigno,Nombre) VALUES (0,"Tauro"); 
INSERT INTO signosZodiaco(idSigno,Nombre) VALUES (0,"Capricornio"); 
INSERT INTO signosZodiaco(idSigno,Nombre) VALUES (0,"Acuario"); 

INSERT INTO tiposContacto(idTipo, Nombre) VALUES(0,"Familiar");
INSERT INTO tiposContacto(idTipo, Nombre) VALUES(0,"Trabajo");
INSERT INTO tiposContacto(idTipo, Nombre) VALUES(0,"Amigo");
INSERT INTO tiposContacto(idTipo, Nombre) VALUES(0,"Otro");

